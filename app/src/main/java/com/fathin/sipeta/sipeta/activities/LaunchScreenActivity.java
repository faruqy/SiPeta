package com.fathin.sipeta.sipeta.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class LaunchScreenActivity extends AppCompatActivity {
    protected int _splashTime = 2500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Intent i = new Intent(LaunchScreenActivity.this, MapsActivity.class);
                startActivity(i);
                finish();
            }
        }, _splashTime);
    }
}
