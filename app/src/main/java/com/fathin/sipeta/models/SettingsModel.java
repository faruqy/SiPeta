package com.fathin.sipeta.models;

import com.google.firebase.database.PropertyName;

public class SettingsModel {

    public static final String FB_ROOT_SETTINGS = "settings";

    @PropertyName("pendaftar")
    private boolean pendaftar;

    @PropertyName("ditolak")
    private boolean ditolak;

    @PropertyName("pendaftar_perempuan")
    private boolean pendaftarPerempuan;

    @PropertyName("pendaftar_laki")
    private boolean pendaftarLaki;

    @PropertyName("diterima")
    private boolean diterima;

    public void setPendaftar(boolean pendaftar) {
        this.pendaftar = pendaftar;
    }

    @PropertyName("pendaftar")
    public boolean isPendaftar() {
        return pendaftar;
    }

    public void setDitolak(boolean ditolak) {
        this.ditolak = ditolak;
    }

    @PropertyName("ditolak")
    public boolean isDitolak() {
        return ditolak;
    }

    public void setPendaftarPerempuan(boolean pendaftarPerempuan) {
        this.pendaftarPerempuan = pendaftarPerempuan;
    }

    @PropertyName("pendaftar_perempuan")
    public boolean isPendaftarPerempuan() {
        return pendaftarPerempuan;
    }

    public void setPendaftarLaki(boolean pendaftarLaki) {
        this.pendaftarLaki = pendaftarLaki;
    }

    @PropertyName("pendaftar_laki")
    public boolean isPendaftarLaki() {
        return pendaftarLaki;
    }

    public void setDiterima(boolean diterima) {
        this.diterima = diterima;
    }

    @PropertyName("diterima")
    public boolean isDiterima() {
        return diterima;
    }

    @Override
    public String toString() {
        return "SettingsModel{" +
                "pendaftar=" + pendaftar +
                ", ditolak=" + ditolak +
                ", pendaftarPerempuan=" + pendaftarPerempuan +
                ", pendaftarLaki=" + pendaftarLaki +
                ", diterima=" + diterima +
                '}';
    }
}
