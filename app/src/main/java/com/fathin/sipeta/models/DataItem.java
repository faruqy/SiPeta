package com.fathin.sipeta.models;

import javax.annotation.Generated;

import com.google.firebase.database.PropertyName;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItem {
    public static final String FB_ROOT_ITEMS = "bidikmisi";


    @PropertyName("cityName")
    @SerializedName("cityName")
    private String cityName;

    @PropertyName("persentase_diterima")
    @SerializedName("persentase_diterima")
    private double persentaseDiterima;

    @PropertyName("kmlname")
    @SerializedName("kmlname")
    private String kmlname;

    @PropertyName("pendaftar")
    @SerializedName("pendaftar")
    private long pendaftar;

    @PropertyName("pendaftar_diterima")
    @SerializedName("pendaftar_diterima")
    private long pendaftarDiterima;

    @PropertyName("pendaftar_perempuan")
    @SerializedName("pendaftar_perempuan")
    private long pendaftarPerempuan;

    @PropertyName("pendaftar_ditolak")
    @SerializedName("pendaftar_ditolak")
    private long pendaftarDitolak;

    @PropertyName("pendaftar_laki")
    @SerializedName("pendaftar_laki")
    private long pendaftarLaki;

    @PropertyName("centerLat")
    @SerializedName("centerLat")
    private double centerLat;

    @PropertyName("id")
    @SerializedName("id")
    private String id;

    @PropertyName("centerLong")
    @SerializedName("centerLong")
    private double centerLong;

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @PropertyName("cityName")
    public String getCityName() {
        return cityName;
    }

    @PropertyName("persentase_diterima")
    public void setPersentaseDiterima(double persentaseDiterima) {
        this.persentaseDiterima = persentaseDiterima;
    }

    @PropertyName("persentase_diterima")
    public double getPersentaseDiterima() {
        return persentaseDiterima;
    }

    public void setKmlname(String kmlname) {
        this.kmlname = kmlname;
    }

    @PropertyName("kmlname")
    public String getKmlname() {
        return kmlname;
    }

    public void setPendaftar(long pendaftar) {
        this.pendaftar = pendaftar;
    }

    @PropertyName("pendaftar")
    public long getPendaftar() {
        return pendaftar;
    }

    public void setPendaftarDiterima(long pendaftarDiterima) {
        this.pendaftarDiterima = pendaftarDiterima;
    }

    @PropertyName("pendaftar_diterima")
    public long getPendaftarDiterima() {
        return pendaftarDiterima;
    }

    public void setPendaftarPerempuan(long pendaftarPerempuan) {
        this.pendaftarPerempuan = pendaftarPerempuan;
    }

    @PropertyName("pendaftar_perempuan")
    public long getPendaftarPerempuan() {
        return pendaftarPerempuan;
    }

    public void setPendaftarDitolak(long pendaftarDitolak) {
        this.pendaftarDitolak = pendaftarDitolak;
    }

    @PropertyName("pendaftar_ditolak")
    public long getPendaftarDitolak() {
        return pendaftarDitolak;
    }

    public void setPendaftarLaki(long pendaftarLaki) {
        this.pendaftarLaki = pendaftarLaki;
    }

    @PropertyName("pendaftar_laki")
    public long getPendaftarLaki() {
        return pendaftarLaki;
    }

    public void setCenterLat(double centerLat) {
        this.centerLat = centerLat;
    }

    @PropertyName("centerLat")
    public double getCenterLat() {
        return centerLat;
    }

    public void setId(String id) {
        this.id = id;
    }

    @PropertyName("id")
    public String getId() {
        return id;
    }

    public void setCenterLong(double centerLong) {
        this.centerLong = centerLong;
    }

    @PropertyName("centerLong")
    public double getCenterLong() {
        return centerLong;
    }

    @Override
    public String toString() {
        return
                "DataItem{" +
                        "cityName = '" + cityName + '\'' +
                        ",persentase_diterima = '" + persentaseDiterima + '\'' +
                        ",kmlname = '" + kmlname + '\'' +
                        ",pendaftar = '" + pendaftar + '\'' +
                        ",pendaftar_diterima = '" + pendaftarDiterima + '\'' +
                        ",pendaftar_perempuan = '" + pendaftarPerempuan + '\'' +
                        ",pendaftar_ditolak = '" + pendaftarDitolak + '\'' +
                        ",pendaftar_laki = '" + pendaftarLaki + '\'' +
                        ",centerLat = '" + centerLat + '\'' +
                        ",id = '" + id + '\'' +
                        ",centerLong = '" + centerLong + '\'' +
                        "}";
    }
}