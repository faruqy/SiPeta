package com.fathin.sipeta.sipeta.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fathin.sipeta.models.DataItem;
import com.fathin.sipeta.models.SettingsModel;
import com.fathin.sipeta.sipeta.R;
import com.fathin.sipeta.utils.FirebaseUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.data.kml.KmlContainer;
import com.google.maps.android.data.kml.KmlLayer;
import com.google.maps.android.data.kml.KmlPlacemark;
import com.google.maps.android.data.kml.KmlPolygon;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final int CONST_WIDTH = 36;
    private static final int CONST_HEIGHT = 48;

    private GoogleMap mMap;
    private FirebaseDatabase mDatabase;

    private RelativeLayout rl_info;
    private TextView tv_name;
    private TextView tv_pendaftar;
    private TextView tv_pendaftar_diterima;
    private TextView tv_pendaftar_ditolak;
    private TextView tv_pendaftar_laki;
    private TextView tv_pendaftar_perempuan;
    private TextView tv_diterima;
    private TextView tv_ditolak;
    private Button btn_close;

    private HashMap<String, DataItem> hmDataItem = new HashMap<>();
    private HashMap<String, BitmapDescriptor> hmIconItem = new HashMap<>();
    private Marker previousMarker;
    private String displayedDataItem = "";

    private List<KmlLayer> listKmlLayer = new ArrayList<>();

    private long total_diterima = 44489L;

    private SettingsModel mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        initView();
        initFirebase();
        getSettings();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void getSettings() {
        final DatabaseReference settingsRef = mDatabase.getReference(SettingsModel.FB_ROOT_SETTINGS);
        settingsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    mSettings = dataSnapshot.getValue(SettingsModel.class);
                    applySettings();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void applySettings() {
        if (mSettings != null) {
            if(mSettings.isPendaftar())
                tv_pendaftar.setVisibility(View.VISIBLE);
            else
                tv_pendaftar.setVisibility(View.GONE);

            if(mSettings.isDiterima())
                tv_pendaftar_diterima.setVisibility(View.VISIBLE);
            else
                tv_pendaftar_diterima.setVisibility(View.GONE);

            if(mSettings.isDitolak())
                tv_pendaftar_ditolak.setVisibility(View.VISIBLE);
            else
                tv_pendaftar_ditolak.setVisibility(View.GONE);

            if(mSettings.isPendaftarLaki())
                tv_pendaftar_laki.setVisibility(View.VISIBLE);
            else
                tv_pendaftar_laki.setVisibility(View.GONE);

            if(mSettings.isPendaftarPerempuan())
                tv_pendaftar_perempuan.setVisibility(View.VISIBLE);
            else
                tv_pendaftar_perempuan.setVisibility(View.GONE);
        }
    }

    private void initView() {
        rl_info = findViewById(R.id.rl_info);
        tv_name = findViewById(R.id.tv_name);
        tv_pendaftar = findViewById(R.id.tv_pendaftar);
        tv_pendaftar_diterima = findViewById(R.id.tv_pendaftar_diterima);
        tv_pendaftar_ditolak = findViewById(R.id.tv_pendaftar_ditolak);
        tv_pendaftar_laki = findViewById(R.id.tv_pendaftar_laki);
        tv_pendaftar_perempuan = findViewById(R.id.tv_pendaftar_perempuan);
        tv_diterima = findViewById(R.id.tv_diterima);
        tv_ditolak = findViewById(R.id.tv_ditolak);
        tv_ditolak.setVisibility(View.GONE);

        btn_close = findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDisplay();
            }
        });
    }

    private void initFirebase() {
        mDatabase = FirebaseUtil.getDatabase();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in ITS and move the camera
        //-7.2846396,112.7944021,
        LatLng centerMap = new LatLng(-7.2846396, 112.7944021);
//        float zoomLevel = 18.14f;
        float zoomLevel = 7.7f;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(centerMap, zoomLevel));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(centerMap));


        DatabaseReference itemRef = mDatabase.getReference(DataItem.FB_ROOT_ITEMS);
        itemRef
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        mMap.clear();
                        hmDataItem.clear();
                        loadKml();
                        if (dataSnapshot.exists()) {
                            long current_total = 0;
                            for (DataSnapshot itemSnap : dataSnapshot.getChildren()) {
                                DataItem itemData = itemSnap.getValue(DataItem.class);
                                LatLng newPos = new LatLng(itemData.getCenterLat(), itemData.getCenterLong());

                                MarkerOptions newMarkerOption = new MarkerOptions()
                                        .position(newPos)
                                        .title(itemData.getCityName());

                                double penerimaan = (double) itemData.getPendaftarDiterima() / (double) total_diterima;

                                Log.d("current", "diterima " + itemData.getCityName() + " = " + itemData.getPendaftarDiterima());
                                Log.d("current", "current " + itemData.getCityName() + " = " + String.format("%.5f", penerimaan));

                                BitmapDescriptor iconMarker = null;
                                if (penerimaan >= 0 && penerimaan <= 0.004) {
                                    iconMarker = BitmapDescriptorFactory.fromBitmap(
                                            resizeBitmap("ic_marker_1", CONST_WIDTH, CONST_HEIGHT)
                                    );
                                } else if (penerimaan > 0.004 && penerimaan <= 0.008) {
                                    iconMarker = BitmapDescriptorFactory.fromBitmap(
                                            resizeBitmap("ic_marker_2", CONST_WIDTH, CONST_HEIGHT)
                                    );
                                } else {
                                    iconMarker = BitmapDescriptorFactory.fromBitmap(
                                            resizeBitmap("ic_marker_3", CONST_WIDTH, CONST_HEIGHT)
                                    );
                                }
                                newMarkerOption.icon(iconMarker);

                                if (itemData.getCityName().equals(displayedDataItem)) {
                                    displayInfo(itemData);

//                                    newMarkerOption.icon(
//                                            BitmapDescriptorFactory.fromBitmap(
//                                                    resizeBitmap("ic_marker_choosen", CONST_WIDTH, CONST_HEIGHT))
//                                    );
//
//                                    previousMarker = mMap.addMarker(newMarkerOption);

                                } else {
                                    mMap.addMarker(newMarkerOption);
                                }


                                hmDataItem.put(itemData.getCityName(), itemData);
                                hmIconItem.put(itemData.getCityName(), iconMarker);

                                current_total += itemData.getPendaftar();
                            }
                            Log.d("total", "total = " + current_total);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                displayInfo(hmDataItem.get(marker.getTitle()));

//                drawChoosenMarker(marker);
                return false;
            }
        });

        mMap.setOnInfoWindowCloseListener(new GoogleMap.OnInfoWindowCloseListener() {
            @Override
            public void onInfoWindowClose(Marker marker) {
                closeDisplay();
//                undoPreviousMarker();
            }
        });

    }

    private void undoPreviousMarker() {
        if (previousMarker != null) {
            previousMarker.setIcon(hmIconItem.get(previousMarker.getTitle()));
        }
        previousMarker = null;
    }

    private void drawChoosenMarker(Marker marker) {
        if (previousMarker != null) {
            previousMarker.setIcon(hmIconItem.get(previousMarker.getTitle()));
        }
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(
                resizeBitmap("ic_marker_choosen", CONST_WIDTH, CONST_HEIGHT))
        );
        previousMarker = marker;
    }

    private void loadKml() {
        String[] kmlFilesStr = getResources().getStringArray(R.array.kml_files);
        for (String kmlName : kmlFilesStr) {
            int kmlResourceId = getResources().getIdentifier(kmlName, "raw", getPackageName());
            try {
                KmlLayer kmlLayer = new KmlLayer(mMap, kmlResourceId, getApplicationContext());

                listKmlLayer.add(kmlLayer);
                kmlLayer.addLayerToMap();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("dev", e.getMessage());
            } catch (XmlPullParserException e) {
                e.printStackTrace();
                Log.e("dev", e.getMessage());
            }
        }//end for
    }

    private void closeDisplay() {
        rl_info.setVisibility(View.INVISIBLE);
        displayedDataItem = "";
    }

    private void moveCameraToKml(KmlLayer kmlLayer) {
        //Retrieve the first container in the KML layer
        KmlContainer container = kmlLayer.getContainers().iterator().next();
        //Retrieve a nested container within the first container
        container = container.getContainers().iterator().next();
        //Retrieve the first placemark in the nested container
        KmlPlacemark placemark = container.getPlacemarks().iterator().next();
        //Retrieve a polygon object in a placemark
        KmlPolygon polygon = (KmlPolygon) placemark.getGeometry();
        //Create LatLngBounds of the outer coordinates of the polygon
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : polygon.getOuterBoundaryCoordinates()) {
            builder.include(latLng);
        }

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), width, height, 1));
    }

    private void displayInfo(DataItem item) {
        rl_info.setVisibility(View.VISIBLE);
        String name = item.getCityName();
        double jumDiterima = (double) item.getPendaftarDiterima() / (double) total_diterima * 100.0f;
//        Toast.makeText(this, item.toString(), Toast.LENGTH_SHORT).show();
//        double jumDitolak = 100.0f - jumDiterima;
        tv_name.setText(name);
        tv_pendaftar.setText("Pendaftar : " + item.getPendaftar());
        tv_pendaftar_diterima.setText("Diterima : " + item.getPendaftarDiterima());
        tv_pendaftar_ditolak.setText("Ditolak : " + item.getPendaftarDitolak());
        tv_pendaftar_laki.setText("Pendaftar Laki-laki : " + item.getPendaftarLaki());
        tv_pendaftar_perempuan.setText("Pendaftar Perempuan : " + item.getPendaftarPerempuan());
        tv_diterima.setText("Persentase Penerimaan : " + String.format("%.2f", jumDiterima) + " %");
//        tv_ditolak.setText("Persentase Ditolak : " + jumDitolak + " %");
        displayedDataItem = name;
    }

    // method definition
    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    public Bitmap resizeBitmap(String drawableName, int width, int height) {
        Bitmap imageBitmap = getBitmapFromVectorDrawable(drawableName);
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false);
    }

    public Bitmap getBitmapFromVectorDrawable(String drawableName) {
        int drawableId = getResources().getIdentifier(drawableName, "drawable", getPackageName());
        Drawable drawable = ContextCompat.getDrawable(this, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_setting: {
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
            }
            break;
            default: {

            }
            break;
        }
        return true;
    }
}
