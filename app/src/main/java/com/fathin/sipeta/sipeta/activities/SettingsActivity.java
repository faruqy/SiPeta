package com.fathin.sipeta.sipeta.activities;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.MenuItem;
import android.widget.CompoundButton;

import com.fathin.sipeta.models.SettingsModel;
import com.fathin.sipeta.sipeta.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SettingsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private ActionBar mActionBar;

    private AppCompatCheckBox cb_pendaftar;
    private AppCompatCheckBox cb_diterima;
    private AppCompatCheckBox cb_ditolak;
    private AppCompatCheckBox cb_pendaftar_laki;
    private AppCompatCheckBox cb_pendaftar_perempuan;
    private DatabaseReference mSettingsRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mActionBar = getSupportActionBar();
        mActionBar.setTitle(getResources().getString(R.string.settings));
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);

        initView();
        initFirebase();
    }

    private void initFirebase() {
        mSettingsRef = FirebaseDatabase.getInstance().getReference(SettingsModel.FB_ROOT_SETTINGS);
        mSettingsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    SettingsModel settings = dataSnapshot.getValue(SettingsModel.class);

                    if (settings != null) {
                        cb_pendaftar.setChecked(settings.isPendaftar());
                        cb_diterima.setChecked(settings.isDiterima());
                        cb_ditolak.setChecked(settings.isDitolak());
                        cb_pendaftar_laki.setChecked(settings.isPendaftarLaki());
                        cb_pendaftar_perempuan.setChecked(settings.isPendaftarPerempuan());
                    }

                    initListener();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initListener() {
        cb_pendaftar.setOnCheckedChangeListener(this);
        cb_diterima.setOnCheckedChangeListener(this);
        cb_ditolak.setOnCheckedChangeListener(this);
        cb_pendaftar_laki.setOnCheckedChangeListener(this);
        cb_pendaftar_perempuan.setOnCheckedChangeListener(this);
    }

    private void initView() {
        cb_pendaftar = findViewById(R.id.cb_pendaftar);
        cb_diterima = findViewById(R.id.cb_diterima);
        cb_ditolak = findViewById(R.id.cb_ditolak);
        cb_pendaftar_laki = findViewById(R.id.cb_pendaftar_laki);
        cb_pendaftar_perempuan = findViewById(R.id.cb_pendaftar_perempuan);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
            break;
            default: {

            }
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_pendaftar: {
                mSettingsRef.child("pendaftar").setValue(isChecked);
            }
            break;
            case R.id.cb_diterima: {
                mSettingsRef.child("diterima").setValue(isChecked);
            }
            break;
            case R.id.cb_ditolak: {
                mSettingsRef.child("ditolak").setValue(isChecked);
            }
            break;
            case R.id.cb_pendaftar_laki: {
                mSettingsRef.child("pendaftar_laki").setValue(isChecked);
            }
            break;
            case R.id.cb_pendaftar_perempuan: {
                mSettingsRef.child("pendaftar_perempuan").setValue(isChecked);
            }
            break;
        }
    }
}
